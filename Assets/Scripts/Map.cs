﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Map : MonoBehaviour
{
	public GameObject hexPrefab;

	// Size of map in terms of number of hex tiles
	// This is NOT representative of the amount of 
	// world space that we're going to take up
	// (i.e our tiles mifht be more or less than 1 Unirt World Unit)
	private int width = 64;
	private int height = 40;

	private float xOffset = 1.546f;
	private float zOffset = 0.443f;

	// Start is called before the first frame update
	void Start()
    {

		for (int x = 0; x < width; x++)
		{
			for (int y = 0; y < height; y++)
			{

				float xPos = x * xOffset;

				// Are we on an odd row?
				if (y % 2 == 0)
				{
					xPos += xOffset/2f;
				}

				GameObject hex_go = Instantiate(hexPrefab, new Vector3(xPos, 0, y * zOffset), Quaternion.identity);

				// Name the gameobject somthing sensible
				hex_go.name = "Hex " + x + "_" + y;

				// Make sure the hex is aware of its place on the map
				hex_go.GetComponent<hex>().x = x;
				hex_go.GetComponent<hex>().y = y;

				// For a cleaner hierachy
				hex_go.transform.SetParent(transform);

				hex_go.isStatic = true;
			}

		}
        
    }
}
