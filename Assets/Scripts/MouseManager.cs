﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MouseManager : MonoBehaviour
{
    // Update is called once per frame
    void Update()
    {
		// Debug.Log( "Mouse position " + Input.mousePosition);
		// This only works in orthographic mode, and only gives us the world position on the same plane as the cameras near clippin plane 
		// (i.e. It's not helpful for our application
		// Vector3 worldPoint = Camera.main.ScreenToWorldPoint(Input.mousePosition);
		// Debug.Log("World Point: " + worldPoint);

		Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

		RaycastHit hitInfo;

		if (Physics.Raycast(ray, out hitInfo))
		{
			GameObject ourHitObject = hitInfo.collider.gameObject;

			Debug.Log("Raycast hit: " + ourHitObject.name);

			// We know what we're mousing over.
			// Maybe we want to show a tooltip


			// We could also check to se if we're clicking on the thing

			if (Input.GetMouseButtonDown(0))
			{

			}
		}

		// If this were an FPS, what you'd do is somthing like this:
		// (This fires a ray out of the center of the cameras view)
		// Ray fpsRay = new Ray(Camera.main.transform.position, Camera.main.transform.forward);
    }
}
