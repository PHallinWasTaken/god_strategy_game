﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InteractionSystem : MonoBehaviour
{
	private Vector3 foodUpgrade = new Vector3(0.05f, 0.05f, 0.05f);
	private LayerMask foodMask;

	private void Awake()
	{
		foodMask = LayerMask.GetMask("Food");
	}

	private void OnTriggerEnter(Collider other)
	{
		Debug.Log("Collision");
		if (other.gameObject.layer == 11)
		{
			other.gameObject.SetActive(false);
			EatFood();
		}
	}

	private void EatFood()
	{
		transform.localScale = transform.localScale + foodUpgrade;
	}
}
