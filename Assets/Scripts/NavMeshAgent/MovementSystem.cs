﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.AI;
using UnityEngine;

public class MovementSystem: MonoBehaviour
{
	// Private variables
	private NavMeshAgent navMeshAgent;
	private Vector3 target;
	private LayerMask FoodMask;
	private float lastFrameRemainingDistance;

	// Publically and privately declared arrays
	private GameObject[] predator;
	private GameObject[] prey;
	private Vector3 lastPosition;
	public Collider[] food;


	private void Awake()
	{
		// Declare reference variables
		navMeshAgent = GetComponent<NavMeshAgent>();
		FoodMask = LayerMask.GetMask("Food");
		navMeshAgent.stoppingDistance = 0.25f;
	}

	private void Update()
	{
		food = Physics.OverlapSphere(transform.position, 5f, FoodMask);

		Debug.Log("Remaining distance: " + navMeshAgent.remainingDistance);
		Debug.Log("Stopping distance. " + navMeshAgent.stoppingDistance);
		Debug.Log("Velocity: " + navMeshAgent.velocity.sqrMagnitude);

		if (isMoving() == false && food.Length > 0 && navMeshAgent.pathPending == false)
		{
			Debug.Log("Found at least 1 food!");
			FindClosestFood();
		}

		else if (food.Length == 0)
		{
			
		}
	}

	private void LateUpdate()
	{
		lastFrameRemainingDistance = navMeshAgent.remainingDistance;
	}

	private bool isMoving()
	{
		if (lastFrameRemainingDistance > navMeshAgent.remainingDistance || navMeshAgent.velocity.sqrMagnitude <= 1f)
		{
			return false;
		}
		return true;
	}

	private void FindClosestFood()
	{
		Debug.Log("Finding closest food");
		// Declare local array with values of distances
		float[] _distanceToObject = new float[food.Length];

		// A loop to calculate and store distance to all objects
		for (int i = 0; i < food.Length; i++)
		{
			_distanceToObject[i] = Vector3.Distance(transform.position, food[i].gameObject.transform.position);
		}

		// Local variables to keep track of which index in array that needs to be remembered
		int _closestObject = 0;
		float _closestDistance = 6;

		// A loop to find out the shortest distance
		for (int i = 0; i < _distanceToObject.Length; i++)
		{
			if (_distanceToObject[i] < _closestDistance)
			{
				_closestObject = i;
				_closestDistance = _distanceToObject[i];
			}
		}

		// Update target position
		target = food[_closestObject].transform.position;
		Debug.Log(target);
		UpdateDestination();
	}

	// Randomize which direction the agent will move
	private void RandomizeWanderingTarget()
	{
		
	}

	// Updates the NavMesh destination
	private void UpdateDestination()
	{
		Debug.Log("Updating destination...");
		navMeshAgent.SetDestination(target);
	}
}