﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;
using TMPro;
using Valve.VR;

public class SteamVRGameManager : MonoBehaviour
{
	[SerializeField] private AudioClip[] themes;
	private AudioSource audioSource;
	private MenuState menuState;

	private bool villagerWasHurt = false;
	private bool gameOver = false;
	private bool gameWon = false;
	private bool invasionStarted;
	private int startTextStage = 0;
	private int invasionTextStage = 0;

	[SerializeField] private GameObject startTextObject;
	[SerializeField] private TMP_Text startText;
	[SerializeField] private GameObject level1Object;
	[SerializeField] private TMP_Text level1Text;
	[SerializeField] private GameObject level1UIObject;
	[SerializeField] private GameObject gameOverObject;
	[SerializeField] private TMP_Text gameOverText;
	[SerializeField] private GameObject invasionTextObject;
	[SerializeField] private TMP_Text invasionText;
	[SerializeField] private GameObject invasionLevel;

	public SteamVR_Input_Sources hand;
	public SteamVR_Action_Boolean trigger;

	private void Awake()
	{
		audioSource = GetComponent<AudioSource>();
	}

	private void Start()
	{
		menuState = MenuState.STARTMENU;
		audioSource.clip = themes[0];
		audioSource.Play();
	}

	private void Update()
	{
		switch (menuState)
		{
			case MenuState.STARTMENU:
				if (startTextStage < 2 && trigger.GetStateDown(hand))
				{
					UpdateStartText();
				}
				break;

			case MenuState.INVASIONMENU:
				if (invasionTextStage < 2 && trigger.GetStateDown(hand))
				{
					UpdateInvasionText();
				}
				break;

			case MenuState.GAMEWON:
				if (gameWon && trigger.GetStateDown(hand))
				{
					Application.Quit();
				}
				break;

			case MenuState.GAMEOVER:
				if (villagerWasHurt && trigger.GetStateDown(hand))
				{
					SceneManager.LoadScene(SceneManager.GetActiveScene().name);
				}
				break;

			case MenuState.NOMENU:
				break;

			default:
				Debug.Log("Default state shouldn't be able to happen");
				break;
		}

		if (villagerWasHurt)
		{
			GameOver();
		}

		if (audioSource.clip == themes[1] && audioSource.time >= 162f)
		{
			gameWon = true;
			GameOver();
		}

		if (audioSource.clip ==  themes[0] && audioSource.time >= audioSource.clip.length)
		{
			audioSource.time = 0f;
		}
	}

	private void UpdateStartText()
	{
		if (startTextStage == 0)
		{
			startTextStage++;
			startText.text = "Look out for the villagers talking bubbles and try to help them! \n \n" + "(Trigger to proceed)";
		}

		else if (startTextStage == 1)
		{
			startTextStage++;
			StartLevel1();
			startTextObject.SetActive(false);
			menuState = MenuState.NOMENU;
		}
	}

	private void StartLevel1()
	{
		level1Object.SetActive(true);
		level1UIObject.SetActive(true);
	}

	public void EndLevel()
	{
		StartCoroutine(EndLevel1());
	}

	private void UpdateInvasionText()
	{
		if (invasionTextStage == 0)
		{
			invasionTextStage++;
			invasionTextObject.SetActive(true);
			menuState = MenuState.INVASIONMENU;
		}

		else if (invasionTextStage == 1)
		{
			invasionTextStage++;
			invasionStarted = true;
			invasionLevel.SetActive(true);
			invasionText.text = "Here They come!";
			audioSource.Stop();
			audioSource.clip = themes[1];
			audioSource.Play();
			menuState = MenuState.NOMENU;
		}
	}

	public void GameOver()
	{
		gameOverObject.SetActive(true);
		gameOver = true;

		if (villagerWasHurt)
		{
			gameOverText.text = "GAME OVER \n \n" + "You hurt a villagers tent \n \n" + "(Trigger to restart)";
			menuState = MenuState.GAMEOVER;
		}

		if (gameWon)
		{
			gameOverText.text = "GAME WON \n \n" + "You Defended the village \n \n" + "(Trigger to return to desktop)";
			menuState = MenuState.GAMEWON;

			audioSource.Stop();
			audioSource.clip = themes[0];
			audioSource.Play();
		}
		
		Time.timeScale = 0f;
	}

	private IEnumerator EndLevel1()
	{
		yield return new WaitForSeconds(2f);
		level1Text.text = "Thank you o Thunder God";
		yield return new WaitForSeconds(3f);
		level1Object.SetActive(false);
		level1UIObject.SetActive(false);
		UpdateInvasionText();
	}
}
