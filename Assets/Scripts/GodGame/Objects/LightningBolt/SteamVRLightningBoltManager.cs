﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR;

public class SteamVRLightningBoltManager : MonoBehaviour
{
	public GameObject lightningBoltPrefab;
	public GameObject aimingDot;
	public LayerMask terrainMask;

	private RaycastHit hit;
	private bool isHolding = false;
	private GameObject lineRendererObject;
	private WorkTableManager workTableManager;
	private AudioManager audioManager;
	private SteamVRBlockerManager blockerManager;
	private SteamVRInvasionManager invasionManager;

	[SerializeField] private Transform lineStartTransform;
	[SerializeField] private float explosionForce;
	[SerializeField] private float explosionRadius;
	[SerializeField] private float explosionUpwardsModifier;
	[SerializeField] private Material lineMaterial;

	public SteamVR_Input_Sources hand;
	public SteamVR_Action_Boolean Trigger;

	private void Awake()
	{
		workTableManager = FindObjectOfType<WorkTableManager>();
		audioManager = GetComponent<AudioManager>();
		blockerManager = FindObjectOfType<SteamVRBlockerManager>();
	}

	private void Update()
	{
		if (lineRendererObject != null)
		{
			Destroy(lineRendererObject);
		}

		if (isHolding)
		{
			DrawLine(lineStartTransform.position, transform.position + (-transform.up), Color.red);
			Physics.Raycast(lineStartTransform.position, -transform.up, out hit, 5f, terrainMask);

			if (hit.collider != null)
			{
				aimingDot.SetActive(true);
				aimingDot.transform.position = hit.point;
			}

			else
				aimingDot.SetActive(false); 

			if (Trigger.GetState(hand))
			{
				if (hit.collider != null)
				{
					Explosion();
				}
				Destroy(lineRendererObject);
				workTableManager.SpawnBolt();
				audioManager.PlayLightningEffect();
				Destroy(gameObject);
			}
		}
	}

	private void Explosion()
	{
		Collider[] _blockers;
		_blockers = Physics.OverlapSphere(hit.point, 1f);
		blockerManager = FindObjectOfType<SteamVRBlockerManager>();

		if (_blockers.Length > 0)
		{
			foreach (Collider _collider in _blockers)
			{
				if (_collider.tag == "Blocker")
				{
					if (_collider.GetComponent<SteamVREnemyManager>() != null)
					{
						invasionManager = FindObjectOfType<SteamVRInvasionManager>();
						invasionManager.KillEnemy(_collider.gameObject);
						_collider.GetComponent<SteamVREnemyManager>().RemoveTarget();
					}
					_collider.attachedRigidbody.AddExplosionForce(explosionForce, hit.point, explosionRadius, explosionUpwardsModifier, ForceMode.Impulse);
					
					if (blockerManager != null)
					{
						blockerManager.StartCoroutine(blockerManager.CheckList());
					}
				}
			}
		}
	}

	void DrawLine(Vector3 start, Vector3 end, Color color)
	{
		lineRendererObject = new GameObject();
		lineRendererObject.transform.position = start;
		lineRendererObject.AddComponent<LineRenderer>();
		LineRenderer lr = lineRendererObject.GetComponent<LineRenderer>();
		lr.material = lineMaterial;
		lr.startColor = color;
		lr.endColor = color;
		lr.startWidth = 0.005f;
		lr.endWidth = 0.005f;
		lr.SetPosition(0, start);
		lr.SetPosition(1, end);
	}

	public void OnPickup()
	{
		isHolding = true;
		aimingDot.SetActive(true);
		workTableManager.lightningBolts.Remove(gameObject.transform);
	}

	public void OnRelease()
	{
		isHolding = false;
		aimingDot.SetActive(false);
		Destroy(gameObject, 3f);
		workTableManager.StartCoroutine(workTableManager.RespawnBolt(gameObject));
	}
}