﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OVRLightningBoltManager : MonoBehaviour
{
	public GameObject lightningBoltPrefab;
	public GameObject aimingDot;
	public LayerMask terrainMask;

	private RaycastHit hit;
	private bool isHolding = false;
	private GameObject lineRendererObject;
	private WorkTableManager workTableManager;
	private AudioManager audioManager;
	private OVRBlockerManager blockerManager;
	private OVRInvasionManager invasionManager;

	[SerializeField] private Transform lineStartTransform;
	[SerializeField] private float explosionForce;
	[SerializeField] private float explosionRadius;
	[SerializeField] private float explosionUpwardsModifier;
	[SerializeField] private Material lineMaterial;

	// REPLACED WITH OCULUS INPUT
	//public SteamVR_Input_Sources hand;
	//public SteamVR_Action_Boolean Trigger;

	[SerializeField] private OVRInput.Controller controller;

	private void Awake()
	{
		workTableManager = FindObjectOfType<WorkTableManager>();
		audioManager = GetComponent<AudioManager>();
		blockerManager = FindObjectOfType<OVRBlockerManager>();
	}

	private void Update()
	{
		if (lineRendererObject != null)
		{
			Destroy(lineRendererObject);
		}

		if (isHolding)
		{
			DrawLine(lineStartTransform.position, transform.position + (-transform.up), Color.red);
			Physics.Raycast(lineStartTransform.position, -transform.up, out hit, 5f, terrainMask);

			if (hit.collider != null)
			{
				aimingDot.SetActive(true);
				aimingDot.transform.position = hit.point;
			}

			else
				aimingDot.SetActive(false); 

			if (OVRInput.Get(OVRInput.Axis1D.PrimaryIndexTrigger, controller) == 1f)
			{
				Debug.Log("Triggered");
				if (hit.collider != null)
				{
					Explosion();
				}
				Destroy(lineRendererObject);
				workTableManager.SpawnBolt();
				audioManager.PlayLightningEffect();
				Destroy(gameObject);
			}
		}
	}

	private void Explosion()
	{
		Collider[] _blockers;
		_blockers = Physics.OverlapSphere(hit.point, 1f);
		blockerManager = FindObjectOfType<OVRBlockerManager>();

		if (_blockers.Length > 0)
		{
			foreach (Collider _collider in _blockers)
			{
				if (_collider.CompareTag("Blocker"))
				{
					if (_collider.GetComponent<OVREnemyManager>() != null)
					{
						invasionManager = FindObjectOfType<OVRInvasionManager>();
						invasionManager.KillEnemy(_collider.gameObject);
						_collider.GetComponent<OVREnemyManager>().RemoveTarget();
					}
					_collider.attachedRigidbody.AddExplosionForce(explosionForce, hit.point, explosionRadius, explosionUpwardsModifier, ForceMode.Impulse);
					
					if (blockerManager != null)
					{
						blockerManager.StartCoroutine(blockerManager.CheckList());
					}
				}
			}
		}
	}

	void DrawLine(Vector3 start, Vector3 end, Color color)
	{
		lineRendererObject = new GameObject();
		lineRendererObject.transform.position = start;
		lineRendererObject.AddComponent<LineRenderer>();
		LineRenderer lr = lineRendererObject.GetComponent<LineRenderer>();
		lr.material = lineMaterial;
		lr.startColor = color;
		lr.endColor = color;
		lr.startWidth = 0.005f;
		lr.endWidth = 0.005f;
		lr.SetPosition(0, start);
		lr.SetPosition(1, end);
	}

	public void OnPickup()
	{
		Debug.Log("Picked up");
		isHolding = true;
		aimingDot.SetActive(true);
		workTableManager.lightningBolts.Remove(gameObject.transform);
	}

	public void OnRelease()
	{
		Debug.Log("Released");
		isHolding = false;
		aimingDot.SetActive(false);
		Destroy(gameObject, 3f);
		workTableManager.StartCoroutine(workTableManager.RespawnBolt(gameObject));
	}
}