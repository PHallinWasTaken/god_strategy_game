﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WorkTableManager : MonoBehaviour
{
	public GameObject lightningBoltPrefab;
	public LayerMask workTableMask;

	private RaycastHit hit;
	[SerializeField] private Transform camTransform;
	[SerializeField] private Vector3 farOffset;
	[SerializeField] private Vector3 closeOffset;
	[SerializeField] private Transform[] lightningBoltSpawns;

	public List<Transform> lightningBolts = new List<Transform>();

	private void Update()
	{
		Physics.Raycast(camTransform.position, camTransform.forward, out hit, 3f, workTableMask);

		if (hit.collider != null)
		{
			transform.position = Vector3.Slerp(transform.position, camTransform.position + closeOffset, 0.5f);
		}

		else
		{
			transform.position = Vector3.Slerp(transform.position, camTransform.position + farOffset * 2, 0.5f);
		}
	}

	private void OnTriggerEnter(Collider other)
	{
		if (other.GetComponentInParent<Transform>().CompareTag("LightningBolt") && !isInList(other.transform))
		{
			lightningBolts.Add(other.transform);
		}
	}

	private void OnTriggerExit(Collider other)
	{
		if (isInList(other.transform))
		{
			lightningBolts.Remove(other.transform);
			StartCoroutine(RespawnBolt(other.gameObject));
		}
	}

	private bool isInList(Transform transform)
	{
		if (lightningBolts.Count <= 0)
		{
			return false;
		}

		foreach (Transform _transform in lightningBolts)
		{
			if (_transform == transform)
			{
				return true;
			}

			else
				return false;
		}

		return false;
	}

	public void SpawnBolt()
	{
		Instantiate(lightningBoltPrefab, lightningBoltSpawns[Random.Range(0, 2)]);
	}

	public IEnumerator RespawnBolt(GameObject _lightningBoltObject)
	{
		yield return new WaitForSeconds(3f);
		Destroy(_lightningBoltObject);
		Instantiate(lightningBoltPrefab, lightningBoltSpawns[Random.Range(0, 2)]);
	}
}
