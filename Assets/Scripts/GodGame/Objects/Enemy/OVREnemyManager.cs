﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OVREnemyManager : MonoBehaviour
{
	private bool hasTarget = false;
	private bool canJump = false;
	private Rigidbody rb;

	public float distanceToTarget;

	private OVRGameManager gameManager;

	[SerializeField] private Vector3 target;
	[SerializeField] private float movementSpeed;

	private void Awake()
	{
		rb = GetComponent<Rigidbody>();
		gameManager = FindObjectOfType<OVRGameManager>();
		StartCoroutine(LookAtPointIn(1f));
	}

	private void FixedUpdate()
	{
		if (Vector3.Distance(transform.position, target) < 0.1f)
		{
			gameManager.GameOver();
		}

		if (canJump && hasTarget)
		{
			rb.AddForce(Vector3.up + transform.forward, ForceMode.Impulse);
			StartCoroutine(JumpCooldown());
		}
	}

	public void RemoveTarget()
	{
		hasTarget = false;
	}

	public void SetTarget(GameObject targetObject)
	{
		target = targetObject.transform.position;
		hasTarget = true;
	}

	private IEnumerator JumpCooldown()
	{
		canJump = false;
		yield return new WaitForSeconds(0.75f);
		transform.LookAt(target);
		distanceToTarget = Vector3.Distance(transform.position, target);
		canJump = true;
	}

	private IEnumerator LookAtPointIn(float seconds)
	{
		yield return new WaitForSeconds(seconds);
		transform.LookAt(target);
		canJump = true;
		if (!hasTarget)
		{
			hasTarget = true;
		}
	}
}
