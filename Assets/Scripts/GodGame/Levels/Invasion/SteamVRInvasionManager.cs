﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SteamVRInvasionManager : MonoBehaviour
{
	private int invasionLevel = 1;
	private bool isAttacking = false;

	[SerializeField] private GameObject[] spawners;
	[SerializeField] private List<GameObject> enemiesLeft;
	[SerializeField] private GameObject enemyPrefab;
	[SerializeField] private GameObject target;

	private void Awake()
	{
		SpawnEnemies();
	}

	private void Update()
	{
		if (isAttacking && enemiesLeft.Count <= 0)
		{
			invasionLevel++;
			SpawnEnemies();
		}
	}

	private void SpawnEnemies()
	{
		isAttacking = false;
		int _spawnAmount = Random.Range(1, invasionLevel * 2 + 1);
		StartCoroutine(SpawnEnemiesOverTime(_spawnAmount));
	}

	public void KillEnemy(GameObject enemyObject)
	{
		enemiesLeft.Remove(enemyObject);
	}

	private IEnumerator SpawnEnemiesOverTime(int amount)
	{
		for (int i = 0; i < amount; i++)
		{
			GameObject _spawnedEnemy = Instantiate(enemyPrefab, spawners[Random.Range(0, 3)].transform.position, Quaternion.identity);
			_spawnedEnemy.GetComponent<SteamVREnemyManager>().SetTarget(target);
			enemiesLeft.Add(_spawnedEnemy);
			yield return new WaitForSeconds(0.5f);
		}
		isAttacking = true;
	}
}
