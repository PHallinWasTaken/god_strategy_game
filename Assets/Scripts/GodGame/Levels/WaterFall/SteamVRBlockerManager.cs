﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SteamVRBlockerManager : MonoBehaviour
{
	private SteamVRGameManager gameManager;
	private List<GameObject> blockers = new List<GameObject>();
	[SerializeField] private GameObject startBlocker;

	private void Awake()
	{
		blockers.Add(startBlocker);
		gameManager = FindObjectOfType<SteamVRGameManager>();
	}

	private void OnTriggerEnter(Collider other)
	{
		if (other.CompareTag("Blocker") && !isInList(other.gameObject))
		{
			blockers.Add(other.gameObject);
		}
	}

	private void OnTriggerExit(Collider other)
	{
		if (isInList(other.gameObject))
		{
			blockers.Remove(other.gameObject);
			Destroy(other.gameObject, 2f);
		}

		if (blockers.Count <= 0)
		{
			gameManager.EndLevel();
		}
	}

	private bool isInList(GameObject gameObject)
	{
		foreach (GameObject _gameObject in blockers)
		{
			if (gameObject == _gameObject)
			{
				return true;
			}
		}

		return false;
	}

	public IEnumerator CheckList()
	{
		yield return new WaitForSeconds(0.5f);

		for (int i = 0; i < blockers.Count; i++)
		{
			if (blockers[i] == null)
			{
				blockers.Remove(blockers[i]);
			}
		}
	}
}
