﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioManager : MonoBehaviour
{
	private AudioSource audioSource;
	public AudioClip lightningSFX;
	private GameObject spawnedAudioGameObject;

	public void PlayLightningEffect()
	{
		spawnedAudioGameObject = new GameObject("LightningSFX");
		audioSource = spawnedAudioGameObject.AddComponent<AudioSource>();
		audioSource.clip = lightningSFX;
		audioSource.time = 6f;
		audioSource.Play();
		Destroy(spawnedAudioGameObject, 4f);
	}
}
